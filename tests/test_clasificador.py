#!/usr/bin/python3
# -*- coding: utf-8 -*-

import contextlib
import io
import sys
import unittest
from unittest.mock import patch

from clasificador import (es_correo_electronico, es_entero,
                          es_real, evaluar_entrada, main)


class TestesFloat(unittest.TestCase):
    def test_es_correo_electronico(self):
        self.assertTrue(es_correo_electronico("sergio@urjc.es"))
        self.assertTrue(es_correo_electronico("up123@pepito.com"))
        self.assertTrue(es_correo_electronico("luis.lusi@gmail.com"))
        self.assertFalse(es_correo_electronico("sergio"))
        self.assertFalse(es_correo_electronico("df123,45"))
        self.assertFalse(es_correo_electronico("sergio.montes"))
        self.assertFalse(es_correo_electronico("sergio@montes"))
        self.assertFalse(es_correo_electronico("@montes"))

    def test_es_entero(self):
        self.assertTrue(es_entero("123"))
        self.assertTrue(es_entero("0"))
        self.assertTrue(es_entero("-456"))
        self.assertFalse(es_entero("12.34"))
        self.assertFalse(es_entero("abc"))
        self.assertFalse(es_entero("1.2.3"))

    def test_es_real(self):
        self.assertTrue(es_real("3.14"))
        self.assertTrue(es_real("-2.718"))
        self.assertTrue(es_real("0.0"))
        self.assertFalse(es_real("3,14"))
        self.assertFalse(es_real("abc"))

    def test_evaluar_entrada(self):
        self.assertEqual(evaluar_entrada("correo@example.com"), "Es un correo electrónico.")
        self.assertEqual(evaluar_entrada("123"), "Es un entero.")
        self.assertEqual(evaluar_entrada("3.14"), "Es un número real.")
        self.assertEqual(evaluar_entrada("texto"), "No es ni un correo, ni un entero, ni un número real.")
        self.assertEqual(evaluar_entrada(""), None)


class TestMain(unittest.TestCase):

    def test_real(self):
        with patch.object(sys, 'argv', ['clasificador.py', '2.0']):
            with contextlib.redirect_stdout(io.StringIO()) as o:
                main()
                output = o.getvalue()
        self.assertEqual("Es un número real.\n", output)

    def test_entero(self):
        with patch.object(sys, 'argv', ['clasificador.py', '2']):
            with contextlib.redirect_stdout(io.StringIO()) as o:
                main()
                output = o.getvalue()
        self.assertEqual("Es un entero.\n", output)

    def test_noargs(self):
        with patch.object(sys, 'argv', ['clasificador.py']):
            with self.assertRaises(SystemExit):
                main()


if __name__ == '__main__':
    unittest.main()
